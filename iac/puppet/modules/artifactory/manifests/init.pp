# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include artifactory
class artifactory {
    package { 'git':
        ensure => installed,
        name   => $git,
    }
    package { 'maven':
        ensure => installed,
        name   => $maven,
    }
    file { '/var/www/html/artfactory':
        ensure => directory,
        owner => 'root',
        group => 'root',
    }
}
