server.port=8095
spring.application.name=monitor
eureka.client.registerWithEureka=true
eureka.client.serviceUrl.defaultZone=${EUREKA_URI:http://192.168.33.10:8761/eureka}
