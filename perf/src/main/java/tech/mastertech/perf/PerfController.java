package tech.mastertech.perf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class PerfController {
	
	@Autowired
	private CustomerRepository repository;
	
	@RequestMapping("/")
	public String index() {       
        return "Performance API";
    }
	@RequestMapping("/perf")
	public String perf() {
        return "Performance test.";
    }
	
	@RequestMapping("/perf/fast")
	public String perf_fast() {
        return "Performance test - FAST Request.";
    }
	
	@RequestMapping("/perf/slow")
	public String perf_slow() {
		int cont = 0;
		for (int i=0; i<=1000; i++)
			cont =i;			
        return "Performance test - SLOW Request: " + cont;       
    }
	
	@RequestMapping("/perf/slowest")
	public String perf_slowest() {
		int cont = 0;
		for (int i=0; i<=10000000; i++)
			cont =i;			
        return "Performance test - SLOWEST Request: " + cont;       
    }
	@RequestMapping("/perf/db")
	public String perf_db() {
		//repository.deleteAll();
		// save a couple of customers
		repository.save(new Customer("Alice", "Smith"));
		repository.save(new Customer("Bob", "Smith"));
		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		String resp = "";
		int i = 0;
		for (Customer customer : repository.findAll()) {
			System.out.println(customer);
			resp = resp + customer;
			i = i + 1;
		}
		System.out.println();
		/*
		// fetch an individual customer
		System.out.println("Customer found with findByFirstName('Alice'):");
		System.out.println("--------------------------------");
		System.out.println(repository.findByFirstName("Alice"));
		System.out.println("Customers found with findByLastName('Smith'):");
		System.out.println("--------------------------------");
		for (Customer customer : repository.findByLastName("Smith")) {
			System.out.println(customer);
		}		
		*/
        return i + " registros : " + resp;
    }
}
