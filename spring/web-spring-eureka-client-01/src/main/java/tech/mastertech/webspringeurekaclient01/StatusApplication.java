package tech.mastertech.webspringeurekaclient01;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusApplication {

  @RequestMapping("/info")
  public String info() {
    return "Status API";
  }
  
  @GetMapping(value = "/server")
  public String server() {
	  String data = "";
	  try {
		data = InetAddress.getLocalHost().getHostAddress() +  "/" + InetAddress.getLocalHost().getHostName();
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		data = "Erro";
	}
    return "Info Server: " + data ;
  }
	
  @RequestMapping("/status")
  public String available() {
    return "Server UP";
  }

  @RequestMapping("/version")
  public String checkedOut() {
    return "Version 2.1 - 18/03";
  }

}
