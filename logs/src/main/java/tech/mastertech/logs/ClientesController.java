package tech.mastertech.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class ClientesController {

	Logger logger = LoggerFactory.getLogger(ClientesController.class);
	//import org.slf4j.Logger;
	//import org.slf4j.LoggerFactory;
	@RequestMapping("/logs")
    public String index() {
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message"); 
        return "Howdy! Check out the Logs to see the output...";
    }
	
	@RequestMapping("/logs/aprovado")
    public String aprovado() {
    	logger.trace("cartão Aprovado"); 
    	logger.info("cartão Aprovado"); 
    	return "Aprovado...";
    }
    
    @RequestMapping("/logs/reprovado")
    public String reprovado() {
    	logger.trace("cartão Reprovado"); 
    	logger.info("cartão Reprovado"); 
        return "Reprovado...";
    }

    @RequestMapping("/logs/cancelado")
    public String cancelado() {
        logger.trace("cartão Cancelado");
        logger.info("cartão Cancelado");
        return "Cancelado...";
    }

}
